import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  template: './app.component.html',
  styles: ['./app.component.css']
})

export class AppComponent implements OnInit {

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog) {}

  ngOnInit() 
  {
  }

  import() {
    //importa arquivo csv
  }
}
